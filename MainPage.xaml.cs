﻿using Microsoft.Gaming.XboxGameBar;
using System;
using System.Threading.Tasks;
using Windows.Media.Control;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace NowPlayingBar
{
    public sealed partial class MainPage : Page
    {
        private static GlobalSystemMediaTransportControlsSessionManager manager = null;
        private static GlobalSystemMediaTransportControlsSession session = null;

        public MainPage()
        {
            this.InitializeComponent();
            Task.Run(async () => await this.InitAsync());
        }

        private async Task InitAsync()
        {
            manager = await GlobalSystemMediaTransportControlsSessionManager.RequestAsync();
            manager.CurrentSessionChanged += async (_, __) => await this.UpdateCurrentSession();
            await this.UpdateCurrentSession();
        }

        private async Task UpdateCurrentSession()
        {
            session = manager.GetCurrentSession();

            if (session != null)
            {
                session.MediaPropertiesChanged += async (_, __) => await this.UpdateMetadata();
            }

            await this.UpdateMetadata();
        }

        private async Task UpdateMetadata()
        {
            GlobalSystemMediaTransportControlsSessionMediaProperties properties = null;
            if (session != null)
            {
                properties = await session.TryGetMediaPropertiesAsync();
            }

            await this.Dispatcher.RunAsync(
                CoreDispatcherPriority.Normal,
                async () => await this.UpdateUI(properties)
            );
        }

        private async Task UpdateUI(GlobalSystemMediaTransportControlsSessionMediaProperties properties)
        {
            if (properties == null)
            {
                // FIXME: страшное
                this.albumArt.Source = null;
                this.artist.Text = "";
                this.title.Text = "🎵 play something 🎵";
                return;
            }

            var image = new BitmapImage();
            
            if (properties.Thumbnail != null)
            {
                var stream = await properties.Thumbnail.OpenReadAsync();                
                await image.SetSourceAsync(stream);
            }

            this.albumArt.Source = image;
            this.artist.Text = properties.Artist;
            this.title.Text = properties.Title;
        }

        private async Task UpdateOpacity(double opacity)
        {
            await this.Dispatcher.RunAsync(
                CoreDispatcherPriority.Normal,
                () => this.Opacity = opacity
            );
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter is XboxGameBarWidget widget)
            {
                widget.RequestedOpacityChanged += async (_, __) => await this.UpdateOpacity(widget.RequestedOpacity);
                await this.UpdateOpacity(widget.RequestedOpacity);
            }
        }
    }
}
